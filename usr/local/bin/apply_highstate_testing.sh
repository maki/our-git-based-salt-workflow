#!/usr/bin/bash
# this is for allowing highstate user to run salt highstate on saltmaster with env=staging through sudo

echo Refreshing pillar data
/usr/bin/salt '*' saltutil.refresh_pillar || true
/usr/bin/salt '*' state.apply test=True saltenv=testing pillarenv=testing --state-output=changes --state-verbose=false -t 200 || true