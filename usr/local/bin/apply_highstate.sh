#!/usr/bin/bash
# this is for allowing highstate user to run salt highstate on saltmaster through sudo

echo Refreshing pillar data
/usr/bin/salt '*' saltutil.refresh_pillar || true
echo SALT_TEST=$SALT_TEST
/usr/bin/salt '*' state.apply test=${SALT_TEST:-True} --state-output=changes --state-verbose=false -t 200 || true