This is the repo that goes with the Saltconf21 talk "[Our git based salt workflow](https://docs.google.com/presentation/d/1TKnwsqC_V0PY2mDYkyPoBeN_1b18NHNn7Cs1D_cxIKo)"

This repo suggests a way to run highstate with test=True on the contents of any branch other than production.  For production branch commits to that will cause highstate to run with test=False and actually apply state change.

### Setup 
- need account on saltmaster for gitlab to run highstate.   This account also needs to write to the /srv/ and /srv_testing/ directories.   In this example we assume username is highstate.
- need key pair to allow for gitlab to ssh to saltmaster.  The public key needs to be added to the .ssh/allowed_keys file for highstate user.  The private key goes into the SSH_GITRUNNER_PRIVATE_KEY variable in the gitlab project settings->CI/CD->Variables.
- copy files from etc/salt/master.d/ to /etc/salt/master.d.  This has the saltmaster config changes needed.  We are introducing an environment called testing which is rooted at /srv_testing/   Restart saltmaster for changes to apply.
- copy files form etc/sudoers.d to /etc/sudoers.d/.  This limits this account from running any salt commands to just the highstate commands.

