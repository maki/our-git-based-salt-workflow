python39:
  pkg.installed:
    - pkgs:
      - python3.9
      - python3.9-venv
      - python-apt
